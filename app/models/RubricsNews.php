<?php

class RubricsNews extends \Eloquent {
	protected $fillable = [];

	protected $table = 'rubrics_has_news';

	public $timestamps = false;
}
